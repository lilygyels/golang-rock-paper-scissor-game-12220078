package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	winScore   = 3 // the score needed to win a level
	levelCount = 3 // the number of levels
	rock       = 1
	paper      = 2
	scissors   = 3
)

type gameState struct {
	playerScore  int
	cpuScore     int
	currentLevel int
}

func (g *gameState) playRound(playerChoice int) {
	cpuChoice := rand.Intn(3) + 1

	fmt.Println("You chose", choiceName(playerChoice))
	fmt.Println("CPU chose", choiceName(cpuChoice))
	fmt.Println()

	// determine the winner
	if playerChoice == cpuChoice {
		fmt.Println("It's a tie!")
	} else if (playerChoice == rock && cpuChoice == scissors) ||
		(playerChoice == paper && cpuChoice == rock) ||
		(playerChoice == scissors && cpuChoice == paper) {
		fmt.Println("You win!")
		fmt.Println()
		g.playerScore++
	} else {
		fmt.Println("CPU wins!")
		fmt.Println()
		g.cpuScore++
	}

	// check if the level is over
	if g.playerScore >= winScore {
		fmt.Println("You win the level!")
		fmt.Println()
		g.playerScore = 0
		g.cpuScore = 0
		g.currentLevel++

		if g.currentLevel > levelCount {
			fmt.Println("You beat the game!")
			fmt.Println()
			return
		}

		fmt.Println("Starting level", g.currentLevel)
	} else if g.cpuScore >= winScore {
		fmt.Println("CPU wins the level!")
		fmt.Println()
		g.playerScore = 0
		g.cpuScore = 0
		fmt.Println("Starting level", g.currentLevel)
		fmt.Println()
	} else {
		fmt.Println("Current score: You", g.playerScore, "- CPU", g.cpuScore)
		fmt.Println()
	}
}

func choiceName(choice int) string {
	switch choice {
	case rock:
		return "rock"
	case paper:
		return "paper"
	case scissors:
		return "scissors"
	default:
		return "invalid choice"
	}
}

func main() {
	fmt.Println("Welcome to Rock-Paper-Scissors!")

	rand.Seed(time.Now().UnixNano())
	game := &gameState{}

	for game.currentLevel <= levelCount {
		fmt.Println("Starting level", game.currentLevel)

		for game.playerScore < winScore && game.cpuScore < winScore {
			fmt.Println("Choose your weapon:")
			fmt.Println("1) Rock")
			fmt.Println("2) Paper")
			fmt.Println("3) Scissors")
			fmt.Println()

			var choice int
			fmt.Scanln(&choice)
			fmt.Println()

			if choice < 1 || choice > 3 {
				fmt.Println("Invalid choice, please try again.")
				continue
			}

			game.playRound(choice)
		}
	}
}
